#!/usr/bin/env bash
set -e
if [ -z ${PYTHON+x} ]; then
    PYTHON=python
fi

if ! command -v virtualenv &> /dev/null; then
    virtualenv --python=`which "$PYTHON"` venv
else
    $PYTHON -m venv venv
fi

# Set up python environment
source venv/bin/activate
pip install -U pip
pip install -r requirements.txt

# Set up node environment
nodeenv -p
deactivate
source venv/bin/activate

# Install required node packages
npm install -g less

# Get pelican-plugins
if [ ! -d "pelican-plugins" ]; then
    git clone https://github.com/getpelican/pelican-plugins.git
fi

# Get the content repository
if [ ! -d "content" ]; then
    git clone git@gitlab.com:pganssle/blog.ganssle.io-content.git content
fi
