#!/usr/bin/env python3
import json
import os

import requests

from urllib.parse import urljoin

THEME_BASE_PATH = 'theme'
THEME_PATH = os.path.join(THEME_BASE_PATH, 'bulrush')
EXTRAS_PATH = os.path.join(os.path.join(THEME_PATH, 'static'), 'vendor')
EXTRA_ASSETS = os.path.join(THEME_BASE_PATH, 'extra_assets.json')

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 ' +
                  '(KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'
}


def download_asset(base, loc):
    full_url = urljoin(base, loc)
    local_path = os.path.join(EXTRAS_PATH, loc)

    r = requests.get(full_url, headers=headers)

    local_dir = os.path.split(local_path)[0]

    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

    with open(local_path, 'wb') as f:
        f.write(r.content)


def download_extra_assets():
    with open(EXTRA_ASSETS, 'r') as f:
        assets = json.load(f)

    for base, loc in assets:
        download_asset(base, loc)


if __name__ == '__main__':
    download_extra_assets()
