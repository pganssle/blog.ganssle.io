#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import sys; sys.path.insert(0, 'theme')
import os

import bulrush

AUTHOR = 'Paul Ganssle'
SITENAME = 'Paul Ganssle'
SITEURL = 'https://blog.ganssle.io'
TIMEZONE = 'America/New_York'
DEFAULT_LANG = 'en'

LICENSE = "CC-BY-4.0"

PATH = 'content'
OUTPUT_PATH = 'html/'

EXTRA_PATH_METADATA = {
    'static/extras/favicon.ico': {'path': 'favicon.ico'}
}

FILENAME_METADATA=''

ARTICLE_FOLDER = 'articles/{date:%Y}/{date:%m}'
ARTICLE_URL = f'{ARTICLE_FOLDER}/{{slug}}.html'
ARTICLE_SAVE_AS = ARTICLE_URL
ARTICLE_LANG_URL = f'{ARTICLE_FOLDER}/{{slug}}-{{lang}}.html'
ARTICLE_LANG_URL_SAVE_AS = ARTICLE_LANG_URL

ARCHIVES_SAVE_AS = 'archives.html'

# Static content
STATIC_PATHS = ['code']

# Theme configuration
THEME = bulrush.PATH
JINJA_ENVIRONMENT = bulrush.ENVIRONMENT
JINJA_FILTERS = bulrush.FILTERS

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['assets', 'code_include']

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
WITH_FUTURE_DATES = False

DISPLAY_PAGES_ON_MENU = True

# Social widget
SOCIAL = (('Homepage', 'https://ganssle.io'),
          ('Github', 'https://github.com/pganssle'),
          ('Mastodon', 'https://qoto.org/@pganssle'),
          ('Twitter', 'https://twitter.com/pganssle'),)

TWITTER_USERNAME = 'pganssle'

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
