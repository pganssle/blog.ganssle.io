#!/usr/bin/env bash
if [ ! -d "venv" ]; then
    . initialize.sh
fi

source venv/bin/activate

# Update the content
cd content
git pull origin master
cd ..

make html

deactivate