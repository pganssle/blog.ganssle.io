#! /usr/bin/env python3 -O

# This needs to be run with -O because there's some erroneous assertion in
# the fontTools library.
import re
import shutil
import os

import pathlib
import tempfile
import subprocess

from fontTools import subset
from fontTools.merge import Merger

DESIRED_ICONS = [
    'rss',
    'globe',
    'github',
    'css3',
    'html5',
    'link',
    'creative-commons',
    'twitter',
    'book',
]

FONT_FAMILY = "FontAwesomeSubset"

FONT_DEFINITION = f"""@font-face {{{{
  font-family: '{FONT_FAMILY}';
  src:
{{flavors}};
}}}}
"""

CSS_START = """.fa,
.fas,
.far,
.fal,
.fab {
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  display: inline-block;
  font-style: normal;
  font-variant: normal;
  text-rendering: auto;
  line-height: 1;
  font-family: 'FontAwesomeSubset'; }
"""

CSS_BASE = """.fa-{icon}:before {{
    content: "\\{codepoint}"; }}
"""


def extract_font_awesome(icon, css):
    name = f'.fa-{icon}'

    m = re.search(name + ':before {\s+content: '
                  '[\'"]+(?P<codepoint>[^\'"]+)',
                  css,
                  re.MULTILINE)

    if m is None:
        raise ValueError(f'Unknown icon: {icon}')

    codepoint = m.group('codepoint')
    if codepoint.startswith('\\'):
        codepoint = codepoint[1:]

    return codepoint


def load_codepoints(css_file, icons=DESIRED_ICONS):
    with open(css_file, 'r') as f:
        css = f.read()
        codepoints = {icon: extract_font_awesome(icon, css)
                      for icon in icons}

    if 'rss' in codepoints:
        # Apparently uBlock is blocking .fa-rss (at least for me)
        codepoints['rss-mod'] = codepoints['rss']
        del codepoints['rss']

    return codepoints

def generate_css(codepoints, font_flavors, font_locs='../fonts/'):
    font_flavors_in = [(font_locs + output_name, flavor)
                       for output_name, flavor in font_flavors]

    font_inputs = ',\n'.join([
        f"    url('{font_loc}') format('{flavor}')"
        for font_loc, flavor in font_flavors_in
    ])

    css = [FONT_DEFINITION.format(flavors=font_inputs), CSS_START]

    css += [CSS_BASE.format(icon=icon, codepoint=codepoint)
            for icon, codepoint in codepoints.items()]

    return '\n'.join(css)


def generate_subset_font(input_fonts, codepoints, output_loc,
                         flavors=['woff2', 'woff']):
    codepoints_str = ','.join(('U+' + cp) for cp in codepoints.values())

    with tempfile.TemporaryDirectory() as tdir:
        # Create subsets of all the input fonts
        font_outputs = []
        for font_in in input_fonts:
            font_path = font_in.as_posix()
            out_name = (font_in.stem  + '.sub' + font_in.suffix)
            font_out = os.path.join(tdir, out_name)

            subset.main(args=(font_path,
                              f'--output-file={font_out}',
                              f'--unicodes={codepoints_str}',
                              '--flavor=woff2'))

            font_outputs.append(font_out)

        # Merge them into a single font output
        merger = Merger()
        font = merger.merge(font_outputs)
        flavors_out = []
        for flavor in flavors:
            font.flavor = flavor
            out_path = pathlib.Path(output_loc.as_posix() + '.' + flavor)
            flavors_out.append((out_path.name, flavor))
            font.save(out_path)

        return flavors_out


if __name__ == "__main__":
    INPUT_LOC = pathlib.Path('theme/fontawesome/')
    OUTPUT_LOC = pathlib.Path('theme/bulrush/static/')

    CSS_FILE = INPUT_LOC / 'css/fontawesome-all.css'
    CSS_LOC = OUTPUT_LOC / 'css/fontawesome-subset.css'

    INPUT_FONTS = [
        'fa-brands-400.woff2',
        'fa-solid-900.woff2'
    ]

    INPUT_FONTS = [INPUT_LOC / 'webfonts' / font for font in INPUT_FONTS]
    OUTPUT_FONT = pathlib.Path('theme/bulrush/static/fonts/font-awesome-subset')

    codepoints = load_codepoints(CSS_FILE)
    
    # Generate the webfonts for the relevant subset
    font_flavors = generate_subset_font(INPUT_FONTS, codepoints, OUTPUT_FONT)
    
    # Generate the CSS for the relevant subset
    css = generate_css(codepoints, font_flavors)

    with open(CSS_LOC, 'w') as f:
        f.write(css)

